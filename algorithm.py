from datetime import datetime, timedelta

__author__    = "Aaron Goidel, Mark Apinis, and James Modica"
__credits__   = ["Aaron Goidel", "Mark Apinis", "James Modica", "Tim Lynch"]
__version__   = "1.0.1"
__status__    = "Prototype"


def getTimeLeft(dueDate, dateFormat="%Y-%m-%d") -> int:
    """
    Takes date the assigenment is due and calculates how long there is until it is due
    :param dueDate: date the assignment is due
    :param dateFormat: format of the date
    :return: how much time there is until the assignment is due
    """

    today = datetime.today() - timedelta(1) # get today's date
    dueDate = datetime.strptime(dueDate, "%Y-%m-%d")  # due date to date type

    return (dueDate - today).days * 24  # return how much time is left in hours



def includeSize(timeLeft, size) -> float:
    """
    Takes time left until due date and size of project and factors the size of the project into the priority
    :param timeLeft: time left until the assignment is due
    :param size: the size of the project in points
    :return: the priority value of the assignment using the date and size of the project
    """

    weightedSize = size * 1.2  # weight the size of the assignment over the time left to do it

    return weightedSize / timeLeft  # calculate priority value


def includeCompleteness(percentDone, timeAndSize) -> float:
    """
    Takes the percentage of the project that has been completed and factors it into the priority value
    :param percentDone: the amount of the assignment already completed (in a percent)
    :param timeAndSize: priority value calculated so far using the date due and the size of the project
    :return: the priority value of the assignment
    """

    completeness = percentDone / 100  # convert percent done into decimal form

    return timeAndSize * (1 - completeness)


def priorityValue(dueDate, points, completeness) -> float:
    """
    Takes input to algorithm and runs all components to find priority value of a task
    :param dueDate: Date the assignment is due
    :param points: how many points it's worth
    :param completeness: how much of the assignment is finished already
    :return: priority value of the assignment
    """
    time = getTimeLeft(dueDate)
    size = includeSize(time, points)
    val = includeCompleteness(completeness, size)
    if val < 0:
        val *= -1
        val += 100

    return val
