#!/usr/bin/env python

__author__    = "Aaron Goidel, Mark Apinis, and James Modica"
__credits__   = ["Aaron Goidel", "Mark Apinis", "James Modica", "Tim Lynch"]
__version__   = "1.0.1"
__status__    = "Prototype"

import algorithm


class Task:
    def __init__(self, title, dueDate, points, completeness):
        self.title = title
        self.dueDate = dueDate
        self.points = points
        self.completeness = completeness
        self.priority = algorithm.priorityValue(dueDate, points, completeness)

    def __repr__(self):
        return repr((self.title, self.priority))

global taskList
taskList = []

print("Instructions...")


def menu():
    print("""
    ----------Pryoritize v1.0.1----------

    1) Create Assignments
    2) List Assignments
    3) Pryoritize
    4) About

    -------------------------------------
            """)


    taskIn = input("What would you like to run?: ")

    if taskIn.lower() in ["1", "create assignments", "create"]:
        toRun = 1
    elif taskIn.lower() in ["2", "list assignments", "list"]:
        toRun = 2
    elif taskIn.lower() in ["3", "pryoritize", "prioritize"]:
        toRun = 3
    elif taskIn.lower() in ["4", "about"]:
        toRun = 4
    else:
        print("That's not an option")
        menu()

    menuOptions[toRun]()


def create():
    global taskList
    creating = True
    while creating:


        name = input("Enter a name for this task: ")
        due = input("When is this due?: ")
        worth = int(input("How many points is this worth?: "))
        done = float(input("How much of this assignment have you already completed?: "))

        taskList.append(Task(name, due, worth, done))

        if input("Continue? (y/n): ").lower() in ["no", "n"]:
            creating = False

    menu()


def list():
    print("-----------------Assignments-----------------")
    for task in taskList:
        print(task.title, "\t", task.dueDate, "\t", task.points, "\t", task.completeness)

    menu()


def sort():
    global taskList
    pryoritized = reversed(sorted(taskList, key=lambda Task: Task.priority))
    for task in pryoritized:
        print("Priority: “, task.title)


def about():
    print("""
    -----------------Pryoritize-----------------
    Written by: %s
    Version: %s
    --------------------------------------------
    Hello, welocome to Pryoritize! This program was designed to tell YOU which assignments you should be working on.
    By simply telling us the due date, weight, and percent completed of all of your current assignments we can tell
    you the order in which you should work on them
    """ %(__author__, __version__))
    menu()


menuOptions = {
        1 : create,
        2 : list,
        3 : sort,
        4 : about
    }


def main():
    menu()


if __name__ == "__main__":
    menu()
